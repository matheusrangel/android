package com.example.a5920396.autonomia;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucas on 14/05/2017.
 */

public class AbastecimentoAdapter extends RecyclerView.Adapter {

    private List<Abastecimento> listaAbastecimento;
    private Context context;
    private FragmentoListagem.OnItemSelectedListener tratadorDeClique;

    public AbastecimentoAdapter(FragmentoListagem.OnItemSelectedListener tratadorDeClique, Context context){
        this.tratadorDeClique = tratadorDeClique;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View elementoPrincipal = LayoutInflater.from( parent.getContext() ).inflate(R.layout.gaveta, null);
        AbastecimentoHolder gaveta = new AbastecimentoHolder(elementoPrincipal, tratadorDeClique);
        return gaveta;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AbastecimentoHolder AbastecimentoHolder = (AbastecimentoHolder) holder;
        AbastecimentoHolder.atualizarInfoGaveta( Abastecimento.getAbastecimentos(this.context).get(position) );
    }

    @Override
    public int getItemCount() {
        return Abastecimento.getAbastecimentos(this.context).size();
    }
}
