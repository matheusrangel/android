package com.example.a5920396.autonomia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by matheus on 5/21/17.
 */

public class DBhelper extends SQLiteOpenHelper {
    private static final String database = "banco.db";
    private static final String table = "abastecimentos";
    private static final String id = "id";
    private static final String data = "data";
    private static final String km = "km";
    private static final String lt = "lt";
    private static final String posto = "posto";
    private static final int version = 2;

    public DBhelper(Context context) {
        super(context, DBhelper.database, null, DBhelper.version);
    }

    @Override
    public void onCreate(SQLiteDatabase connection) {
        String sql = "CREATE TABLE "+table+"("
                + id + " integer primary key autoincrement,"
                + data + " text,"
                + km + " text,"
                + lt + " text,"
                + posto + " text"
                +")";
        connection.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase connection, int oldVersion, int newVersion) {
        connection.execSQL("DROP TABLE IF EXISTS " + table);
        onCreate(connection);
    }

}
