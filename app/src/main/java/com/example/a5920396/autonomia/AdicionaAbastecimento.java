package com.example.a5920396.autonomia;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Lucas on 14/05/2017.
 */

public class AdicionaAbastecimento extends AppCompatActivity {
    private EditText etKmAtual;
    private EditText etLtAbastecido;
    private EditText etData;
    private Spinner spPosto;
    private final Calendar selectedDate = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adicionar_abastecimento);

        etKmAtual = (EditText) findViewById(R.id.etKmAtual);
        etLtAbastecido = (EditText) findViewById(R.id.etLitrosAbastecidos);
        etData = (EditText) findViewById(R.id.etDataAbastecimento);
        spPosto = (Spinner) findViewById(R.id.spPosto);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.bandeiras, R.layout.support_simple_spinner_dropdown_item);
        spPosto.setAdapter(adapter);

    }

    public void datePicker(View v) {
        final Calendar c = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, month);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                DateFormat data = new SimpleDateFormat("dd/MM/yyyy");
                etData.setText(data.format(c.getTime()));
            }
        };
        new DatePickerDialog(this, listener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void onClickSalvar(View v) {
        TextView tvPosto = (TextView) spPosto.getSelectedView();
        String posto = tvPosto.getText().toString();
        Double ultimoKm;
        if(Abastecimento.getSize() > 0){
            Abastecimento ultimoAbastecimento = Abastecimento.listaAbastecimento.get(Abastecimento.getSize()-1);
            ultimoKm = Double.parseDouble(ultimoAbastecimento.getKmPercorrido());
        }else{
            ultimoKm = 0.0;
        }
        Double atualKm = Double.parseDouble(etKmAtual.getText().toString());

        if(atualKm >= ultimoKm){
            Abastecimento novoAbastecimento = new Abastecimento(
                    etData.getText().toString(),
                    etLtAbastecido.getText().toString(),
                    Double.toString(atualKm),
                    posto
            );
            try{
                AbastecimentoDAO dao = new AbastecimentoDAO(this.getApplicationContext());
                dao.save(novoAbastecimento);
            }catch(Exception e){
                Toast.makeText(this.getApplicationContext(), "Falhou", Toast.LENGTH_LONG);
            }
            finish();
        }else{
            etKmAtual.setError("Você não pode informar uma kilometragem menor que a última informada");
        }
    }
}
