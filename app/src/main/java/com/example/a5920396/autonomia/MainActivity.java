package com.example.a5920396.autonomia;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FragmentoListagem.OnItemSelectedListener {

    private TextView tvAutonomia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvAutonomia = (TextView) findViewById(R.id.tvAutonomiaAtual);
        String autonomiaLegivel = getResources().getString(R.string.autonomia, 0.0);
        tvAutonomia.setText(autonomiaLegivel);
        FragmentoListagem fragmentoListagem = new FragmentoListagem();

        FragmentTransaction transacao =  getSupportFragmentManager().beginTransaction();
        transacao.replace(R.id.flFragmento1, fragmentoListagem);
        transacao.commit();
    }

    public void adicionaAbastecimento(View v) {
        Intent intent = new Intent(this.getApplicationContext(), AdicionaAbastecimento.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelect(Abastecimento abastecimentoGaveta) {
        FragmentoDetalhe FragmentoDetalhe = new FragmentoDetalhe();
        Bundle mochila = new Bundle();
        mochila.putSerializable("abastecimento",abastecimentoGaveta);
        FragmentoDetalhe.setArguments(mochila);
        FragmentTransaction transacao =  getSupportFragmentManager().beginTransaction();

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            transacao.replace(R.id.flFragmento2, FragmentoDetalhe);
        }else{
            transacao.replace(R.id.flFragmento1, FragmentoDetalhe);
            transacao.addToBackStack(null);
        }
        transacao.commit();
    }

    @Override
    public void onResume(){
        super.onResume();
        Double autonomia = Carro.calculaAutonomia(this.getApplicationContext());
        tvAutonomia.setText(getResources().getString(R.string.autonomia, autonomia));
        FragmentoListagem fragmentoListagem = new FragmentoListagem();
        FragmentTransaction transacao =  getSupportFragmentManager().beginTransaction();
        transacao.replace(R.id.flFragmento1, fragmentoListagem);
        transacao.commit();
    }
}
