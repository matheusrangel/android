package com.example.a5920396.autonomia;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by matheus on 6/4/17.
 */

public class FragmentoDetalhe extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gaveta, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if(getArguments()==null){
            return;
        }
        TextView tvPosto = (TextView) view.findViewById(R.id.tvPosto);
        TextView tvData = (TextView) view.findViewById(R.id.tvData);
        TextView tvKm = (TextView) view.findViewById(R.id.tvKm);
        TextView tvLitros = (TextView) view.findViewById(R.id.tvLitros);
        ImageView bandeira = (ImageView) view.findViewById(R.id.ivLogo);
        Abastecimento a = (Abastecimento) getArguments().getSerializable("abastecimento");
        tvPosto.setText(a.getPosto());
        tvData.setText(a.getData());
        tvKm.setText(a.getKmPercorrido());
        tvLitros.setText(a.getLtAbastecidos());
        bandeira.setImageResource(this.selecionaBandeira(a.getPosto()));
    }


    public Integer selecionaBandeira(String nomePosto) {
        if(nomePosto.equals("Texaco")) {
            return R.drawable.texaco;
        } else if(nomePosto.equals("Shell")) {
            return R.drawable.shell;
        } else if(nomePosto.equals("Petrobras")) {
            return R.drawable.petrobras;
        } else if(nomePosto.equals("Ipiranga")) {
            return R.drawable.ipiranga;
        } else {
            return R.drawable.outros;
        }
    }
}