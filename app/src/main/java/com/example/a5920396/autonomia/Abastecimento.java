package com.example.a5920396.autonomia;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lucas on 14/05/2017.
 */

public class Abastecimento implements Serializable {

    public static ArrayList<Abastecimento> listaAbastecimento = new ArrayList<>();
    private String data;
    private String posto;
    private String ltAbastecidos;
    private String kmPercorrido;

    public static int getSize(){
        return Abastecimento.listaAbastecimento.size();
    }

    public Abastecimento(String data, String lt, String km, String posto) {
        this.data = data;
        this.ltAbastecidos = lt;
        this.kmPercorrido = km;
        this.posto = posto;
    }

//    public static Cursor getList(){
//        AbastecimentoDAO dao = new AbastecimentoDAO();
//        return
//    }

    public static ArrayList<Abastecimento> getAbastecimentos(Context context){
        AbastecimentoDAO dao = new AbastecimentoDAO(context);
        dao.getCollection();
        return Abastecimento.listaAbastecimento;
    }

    public String getData() {
        return this.data;
    }

    public String getPosto() {
        return posto;
    }

    public String getLtAbastecidos() {
        return this.ltAbastecidos;
    }

    public String getKmPercorrido() {
        return this.kmPercorrido;
    }
}

