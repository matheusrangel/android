package com.example.a5920396.autonomia;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by marcelo on 5/23/17.
 */

public class AbastecimentoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final Context context;

    private FragmentoListagem.OnItemSelectedListener tratadorDeClique;
    private Abastecimento abastecimentoGaveta;
    private ImageView bandeira;
    private TextView posto;
    private TextView data;
    private TextView km;
    private TextView litros;

    public void atualizarInfoGaveta(Abastecimento abastecimento){
        this.posto.setText( String.valueOf( abastecimento.getPosto() ) );
        this.data.setText( abastecimento.getData() );
        this.km.setText( abastecimento.getKmPercorrido() );
        this.litros.setText( abastecimento.getLtAbastecidos() );
        this.abastecimentoGaveta = abastecimento;
        this.selecionaBandeira(abastecimento.getPosto());
    }

    public AbastecimentoHolder(View itemView, FragmentoListagem.OnItemSelectedListener tratadorDeClique) {
        super(itemView);
        context = itemView.getContext();
        bandeira = (ImageView) itemView.findViewById(R.id.ivLogo);
        posto = (TextView) itemView.findViewById(R.id.tvPosto);
        data = (TextView) itemView.findViewById(R.id.tvData);
        km = (TextView) itemView.findViewById(R.id.tvKm);
        litros = (TextView) itemView.findViewById(R.id.tvLitros);
        itemView.setOnClickListener(this);
        this.tratadorDeClique = tratadorDeClique;
    }

    @Override
    public void onClick(View v) {
        this.tratadorDeClique.onItemSelect(this.abastecimentoGaveta);
    }

    public void selecionaBandeira(String nomePosto) {
        if(nomePosto.equals("Texaco")) {
            bandeira.setImageResource(R.drawable.texaco);
        } else if(nomePosto.equals("Shell")) {
            bandeira.setImageResource(R.drawable.shell);
        } else if(nomePosto.equals("Petrobras")) {
            bandeira.setImageResource(R.drawable.petrobras);
        } else if(nomePosto.equals("Ipiranga")) {
            bandeira.setImageResource(R.drawable.ipiranga);
        } else {
            bandeira.setImageResource(R.drawable.outros);
        }
    }

}
