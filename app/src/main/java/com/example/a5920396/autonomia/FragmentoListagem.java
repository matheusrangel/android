package com.example.a5920396.autonomia;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by marcelo on 5/22/17.
 */

public class FragmentoListagem extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.listagem, container, false);
        RecyclerView rvLista = (RecyclerView) v.findViewById(R.id.rvLista);
        AbastecimentoAdapter AbastecimentoAdapter = new AbastecimentoAdapter(this.listener, this.getContext());

        rvLista.setLayoutManager(new LinearLayoutManager(this.getContext()));
        rvLista.setAdapter(AbastecimentoAdapter);

        return v;
    }

    private OnItemSelectedListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnItemSelectedListener){
            this.listener = (OnItemSelectedListener) context;
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelect(Abastecimento abastecimentoGaveta);
    }
}
