package com.example.a5920396.autonomia;

import android.content.Context;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Lucas on 14/05/2017.
 */

public class Carro {

    public static ArrayList<Double> autonomia = null;

    public static double calculaAutonomia(Context context){
        ArrayList<Abastecimento> abastecimentos = Abastecimento.getAbastecimentos(context);
        ArrayList<Double> km = new ArrayList<>();
        ArrayList<Double> litros = new ArrayList<>();
        Double autonomia = 0.0;

        if(abastecimentos != null){
            if(abastecimentos.size() > 1) {
                for(Abastecimento _abastecimento : abastecimentos) {
                    km.add(Double.parseDouble(_abastecimento.getKmPercorrido()));
                    litros.add(Double.parseDouble(_abastecimento.getLtAbastecidos()));
                }

                Double totalLitros = 0.0;

                for(int i = 1; i < litros.size(); i++){
                    totalLitros += litros.get(i);
                }

                Double kmInicial = km.get(0);
                Double kmFinal = km.get(km.size()-1);
                Double totalPercorrido = kmFinal - kmInicial;
                autonomia = totalPercorrido / totalLitros;
            }
        }
        return autonomia;
    }
}
