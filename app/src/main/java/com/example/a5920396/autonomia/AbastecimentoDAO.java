package com.example.a5920396.autonomia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by matheus on 5/21/17.
 */

public class AbastecimentoDAO {
    private static final String database = "banco.db";
    private static final String table = "abastecimentos";
    private static final String id = "id";
    private static final String data = "data";
    private static final String km = "km";
    private static final String lt = "lt";
    private static final String posto = "posto";
    private final DBhelper connection;

    public AbastecimentoDAO(Context context){
        this.connection = new DBhelper(context);
    }

    public boolean save(Abastecimento abastecimento){
        ContentValues valores;
        SQLiteDatabase db = this.connection.getReadableDatabase();
        long resultado;

        valores = new ContentValues();
        valores.put(this.data, abastecimento.getData());
        valores.put(this.km, abastecimento.getKmPercorrido());
        valores.put(this.lt, abastecimento.getLtAbastecidos());
        valores.put(this.posto, abastecimento.getPosto());

        resultado = db.insert(this.table, null, valores);
        this.getCollection();
        connection.close();

        return resultado ==-1;

    }

    public void getCollection(){
        Cursor cursor;
        String[] campos =  {this.data, this.km, this.lt, this.posto};
        SQLiteDatabase db = this.connection.getReadableDatabase();
        cursor = db.query(
                this.table,
                campos,
                null,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor.moveToFirst()) {
            String data, km, lt, posto;
            Abastecimento.listaAbastecimento = new ArrayList<>();
            do {
                data = cursor.getString(cursor.getColumnIndex(this.data));
                km = cursor.getString(cursor.getColumnIndex(this.km));
                lt = cursor.getString(cursor.getColumnIndex(this.lt));
                posto = cursor.getString(cursor.getColumnIndex(this.posto));
                Abastecimento abastecimento = new Abastecimento(data, lt, km, posto);
                Abastecimento.listaAbastecimento.add(abastecimento);
            } while (cursor.moveToNext());
        }

        connection.close();
    }
}
